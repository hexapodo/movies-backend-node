'use strict'

var express = require('express');
var moviesController = require('./movies');

var api = express.Router();

api.get('/movie/year/:year', moviesController.getByYear);
api.get('/movie/:id', moviesController.getById);

module.exports = api;