'use strict'

var express = require('express');
var ranksController = require('./ranks');

var api = express.Router();

api.delete('/rank/:id', ranksController.rankDelete);
api.post('/rank', ranksController.rankCreation);
api.put('/rank/:id', ranksController.rankModification);

module.exports = api;