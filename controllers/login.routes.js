'use strict'

var express = require('express');
var loginController = require('./login');

var api = express.Router();

api.post('/login', loginController.login);

module.exports = api;