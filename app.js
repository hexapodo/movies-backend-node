'use strict'

var config = require('./config');
var express = require('express');
var app = express();
var jwtService = require('./jwt');

var loginRoutes = require('./controllers/login.routes');
var moviesRoutes = require('./controllers/movies.routes');
var ranksRoutes = require('./controllers/ranks.routes');

app.use(express.json());

app.use('/api', loginRoutes);
app.use('/api', moviesRoutes);
app.use('/api', ranksRoutes);

app.listen(3000, function () {
  console.log('Movies backend running on port 3000!');
});
