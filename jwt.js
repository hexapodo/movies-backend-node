'use strict'

var config = require('./config');
var jwt = require('jsonwebtoken');


function extractData(token) {
    try {
        const data = jwt.verify(token, config.jwtKey);
        return data.custom;
    } catch(e) {
        return null;
    }
}

module.exports = {
    extractData
};
